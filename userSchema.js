var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var envVars = require('./vars.js');

var userSchema = new Schema({
    twitterID: { type: Number },
    twitterToken: { type: String },
    twitterSecret: { type: String },
    localUsername: { type: String, unique: true }
});

var mongooseUser = envVars.mongooseUser;
var mongoosePass = envVars.mongoosePass;
var mongooseHostName = envVars.mongooseHostName;
mongoose.connect('mongodb://' + mongooseUser + ':' + mongoosePass + '@' + mongooseHostName);

module.exports.userModel = mongoose.model('user', userSchema);
