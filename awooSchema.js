var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var envVars = require('./vars.js');

var caseEnum = {
    values: ['lowerCase', 'titleCase', 'inverseTitleCase', 'wiggleCase', 'upperCase', 'smallCaps', 'upsideDown'],
    message: 'error - {PATH}: {VALUE}'
}

var spectrumSchema = new Schema({
    start: { type: String, match: /[0-9a-fA-F]{6}/, default: "FFDCFC" },
    end: { type: String, match: /[0-9a-fA-F]{6}/, default: "C8FFEF" },
})

var colourSchema = new Schema({
    type: { type: String, match: /solid|spectrum/, default: "solid"},
    solidValue: { type: String, match: /[0-9a-fA-F]{6}/, default: "000000"},
    spectrumValue: { type: spectrumSchema }
});

var awooSchema = new Schema({
    awootedAt: { type: Date, default: Date.now },
    fromUser: { type: String, default: "" },
    toUser: { type: String, default: "" },
    size: { type: Number, min: 6, max: 32, default: 12 },
    affix: { type: [String], default: [] },
    content: { type: String, match: /awo[o]+/, default: "awoo" },
    decoration: { type: String, default: "" },
    case: { type: String, enum: caseEnum , default: "smallCaps" },
    colour: { type: colourSchema }
});

var mongooseUser = envVars.mongooseUser;
var mongoosePass = envVars.mongoosePass;
var mongooseHostName = envVars.mongooseHostName;
mongoose.connect('mongodb://' + mongooseUser + ':' + mongoosePass + '@' + mongooseHostName);

module.exports.awootModel = mongoose.model('awoot', awooSchema);
