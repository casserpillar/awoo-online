var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var mongooseConfig = require("../awooSchema");
var Awoot = mongooseConfig.awootModel;
var awootRenderer = require("../public/javascripts/renderAwoo.js");

/* GET home page. */
router.get('/', function(req, res, next) {
    
    // awooList = [{
    //     awootedAt: Date.now,
    //     fromUser: "Casserpillar",
    //     toUser: "Pupperfly",
    //     sizeEm: 2,
    //     affix: ["x","X","*","~"],
    //     content: "awoooo",
    //     decoration: "🐕🐕🐕🐕🐕",
    //     case: "wiggleCase",
    //     colour: {type: "spectrum", solidValue: "FFDCFC", spectrumValue: { start: "FFDCFC", end: "C8FFEF"}}
    // }]

    // res.render('index', {awooList: awooList});
    Awoot.
        find().
        sort({awootedAt: 'descending'}).
        limit(10).
        exec(function (err, awooList) {

            if (err) {
                res.render('index')
            } 
            else { 
                    for (i=0; i<awooList.length; i++) {
        awooList[i].rendered = awootRenderer.renderAwoo(awooList[i]);
    }
                // renderIndex('index', {awooList: awooList});
                res.render('index', {awooList: awooList});
            }
        });
        renderIndex = function (view, dict) {
    res.render(view, dict); 
}
});




module.exports = router;

// awootedAt: { type: Date, default: Date.now},
// fromUser: { type: String, default: ""},
// toUser: { type: String, default: ""},
// awooSize: { type: Number, min: 6, max: 32, default: 12},
// awooAffix: { type: [String], default: [] },
// awooContent: {type: String, match: /awo[o]+/, default: "awoo"},
// awooDecoration: {type: String, default: ""},
// awooCase: {type: String, enum: awooCaseEnum , default: "smallcaps"},
// awooColour: { type: colourSchema}
//
// also added to simplify the pug:
// awooRendered - prefix + rendered case-set awooContent + suffix + decoration
